<cfcomponent bindingname="wrikeAttachment" hint="Integrate with Wrike Attachment API." extends="api">
	
	<cfset this.name = "wrikeAttachment" />
	
	<!---
		CREATE ATTACHMENTS
		@cite https://developers.wrike.com/documentation/api/methods/create-wrike-attachment
	--->
	<cffunction name="new" access="public" returnType="struct" output="false">
		<cfargument name="taskid" required="true" type="string" />
		<cfargument name="body" required="true" type="string" hint="Should be a binary file object / string, I think. Use fileReadBinary(url)." />
		<cfargument name="filename" required="true" type="string" hint="Duh, what should we call this attachment?" />
		
		<cfhttp method="POST" url="#application.wrikeURI#/tasks/#arguments.taskid#/attachments" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
			<cfhttpparam type="Header" name="X-Requested-With"  value="XMLHttpRequest" />
			<cfhttpparam type="Header" name="X-File-Name"  value="#arguments.filename#" />
			<cfhttpparam type="Header" name="content-type"  value="application/octet-stream" />
			<cfhttpparam type="body" value="#arguments.body#">
		</cfhttp>
			
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>