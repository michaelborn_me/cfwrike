<cfcomponent bindingname="wrikeComment" hint="Integrate with Wrike Users API." extends="api">
	
	<!---
		GET ALL COMMENTS or get comments by single account
		/comments
		/accounts/{accountid}/comments
		
		@cite https://developers.wrike.com/documentation/api/methods/get-comments
	--->
	<cffunction name="get" returnType="struct" access="public" output="false">
		<cfargument name="accountid" required="false" type="string" default="" hint="Wrike Account ID." />
		
		<cfif arguments.accountid GT "">
			<cfset local.response = this.getByAccount(arguments.accountid)>
		<cfelse>
			<cfset local.response = this.getAll()>
		</cfif>
		
		<cfreturn local.response />
	</cffunction>
	
	<!---
		GET COMMENTS by a single account
		/accounts/{accountid}/comments
		
		@cite https://developers.wrike.com/documentation/api/methods/get-comments
	--->
	<cffunction name="getByAccount" returnType="struct" access="private" output="false">
		<cfargument name="accountid" required="true" type="string" hint="Wrike User ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/accounts/#arguments.accountid#/comments" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
	
	<!---
		GET ALL COMMENTS
		/comments
		
		@cite https://developers.wrike.com/documentation/api/methods/get-comments
	--->
	<cffunction name="getAll" returnType="struct" access="private" output="false">
		
		<cfhttp method="GET" url="#application.wrikeURI#/comments" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>