<cfcomponent bindingname="wrikeUser" hint="Integrate with Wrike Users API." extends="api">
	
	<!---
		LIST USERS
		@cite https://developers.wrike.com/documentation/api/methods/query-user
	--->
	<cffunction name="get" returnType="struct" access="public" output="false">
		<cfargument name="userid" required="true" type="string" hint="Wrike User ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/users/#arguments.userid#" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization"  value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>