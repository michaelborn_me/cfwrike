/**
 * Integrate with Wrike Folders API.
 */
component bindingname="wrikeFolder" hint="Integrate with Wrike Folders API." extends="api" {
  /* 
    LIST TIME LOGS
    @cite https://developers.wrike.com/documentation/api/methods/query-timelogs
  */

  public struct function get(
  ) output=true
  {
    local.params = {};
    
    return _get(
      url = "/folders",
      params = local.params
    );
  }
  public struct function getByAccount(
      required string accountid,
      string permalink,
      boolean descendants,
      struct metadata,
      struct customField,
      struct updatedDate,
      boolean project,
      boolean deleted,
      array fields
  ) output=true
  {
    local.params = {};
    if (structKeyExists(arguments, "permalink")) { local.params["permalink"] = arguments.permalink; }
    if (structKeyExists(arguments, "descendants")) { local.params["descendants"] = arguments.descendants; }
    if (structKeyExists(arguments, "metadata")) { local.params["metadata"] = arguments.metadata; }
    if (structKeyExists(arguments, "customField")) { local.params["customField"] = arguments.customField; }
    if (structKeyExists(arguments, "updatedDate")) { local.params["updatedDate"] = arguments.updatedDate; }
    if (structKeyExists(arguments, "project")) { local.params["project"] = arguments.project; }
    if (structKeyExists(arguments, "deleted")) { local.params["deleted"] = arguments.deleted; }
    if (structKeyExists(arguments, "fields")) { local.params["fields"] = arguments.fields; }

    return _get(
      url = "/accounts/#arguments.accountid#/folders",
      params = local.params
    );
  }
}