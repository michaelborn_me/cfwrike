<cfcomponent bindingname="wrikeContact" hint="Integrate with Wrike Contacts API." extends="api">
	<cfset this.name = "wrikeContact" />

	<!---
		LIST CONTACTS
		@cite https://developers.wrike.com/documentation/api/methods/query-contacts
	--->
	<cffunction name="get" returnType="struct" access="public" output="false">
		<cfargument name="accountid" required="false" type="string" default="" hint="Wrike Account ID." />

    <cfif arguments.accountid GT "">
      <cfset local.retval = getByAccount( argumentCollection = arguments ) />
    <cfelse>
      <cfset local.retval = getAll( argumentCollection = arguments ) />
    </cfif>
		
		<cfreturn local.retval />
	</cffunction>

	<!---
		LIST CONTACTS
		@cite https://developers.wrike.com/documentation/api/methods/query-contacts
	--->
	<cffunction name="getAll" returnType="struct" access="public" output="false">

		<cfhttp method="GET" url="#application.wrikeURI#/contacts" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization" value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>

	<!---
		LIST CONTACTS BY ACCOUNT ID
		@cite https://developers.wrike.com/documentation/api/methods/query-contacts
	--->
	<cffunction name="getByAccount" returnType="struct" access="public" output="false">
		<cfargument name="accountid" required="true" type="string" hint="Wrike Account ID." />
		
		<cfhttp method="GET" url="#application.wrikeURI#/accounts/#arguments.accountid#/contacts" result="local.cfhttp">
			<cfhttpparam type="Header" name="Authorization" value="Bearer #application.wrikeToken#" />
		</cfhttp>
		
		<cfreturn handleResult(local.cfhttp) />
	</cffunction>
</cfcomponent>
