<html>
<head>
    <title>Wrike Integration Demo</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" rel="stylesheet" />
</head>
<body>
    <div class="hero is-dark">
        <div class="hero-body">
        <div class="container">
            <h1 class="title">Wrike Integration Demo</h1>
        </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
        <div class="columns">
            <div class="column is-one-quarter">
                <nav class="menu" role="navigation" aria-label="main navigation">
                <ul class="menu-list">
                    <li><a class="navbar-item" href="/demos/accounts_get.cfm">Accounts</a></li>
                    <li><a class="navbar-item" href="/demos/groups_get.cfm">Groups</a></li>
                    <li><a class="navbar-item" href="/demos/users_get.cfm">Users</a></li>
                    <li><a class="navbar-item" href="/demos/contacts_get.cfm">Contacts</a></li>
                    <li><a class="navbar-item" href="/demos/timelog_get.cfm">Timelogs</a></li>
                    <li><a class="navbar-item" href="/demos/folders_get.cfm">Folders</a></li>
                    <li><a class="navbar-item" href="/demos/comments_get.cfm">Comments</a></li>
                </ul>
                </nav>
            </div><!---onethird--->
            <div class="column is-three-quarters">