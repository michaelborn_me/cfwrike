component {
	this.name = "wrikeapp";
	
	public void function onApplicationStart() {
		// load secrets from config.cfm
		include "config.cfm";
		
		// load important API settings
		application.wrikeToken = variables.config.wrikeToken;
		application.wrikeURI = variables.config.wrikeURI;
	}
	
	public void function onRequestStart() {
		// useful for development - reload the application and config variables.
		if ( StructKeyExists(url,"reload") ) {
			applicationStop();
		}
	}
    public void function onRequest(targetPage) {
        include "/header.cfm";
        include arguments.targetPage;
        include "/footer.cfm";
    }
	
	public void function onError(struct e, string eventName = "[ERROR]") {
		writeOutput("<h2>#arguments.e.message#</h2>");
		writeDump(e);
	}
}