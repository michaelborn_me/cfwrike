<cfscript>
	// It is EXTREMELY important that config.cfm does not get committed to source code! It should be git-ignored.
	variables.config = {
		// api token for authentication.
		wrikeToken = "DONT_EVEN_THINK_ABOUT_IT",
		
		// base url / domain of the wrike API.
		wrikeURI = "https://www.wrike.com/api/v3"
	}
</cfscript>